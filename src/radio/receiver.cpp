#include "radio/receiver.h"
#include <string.h>

//initalize statics
uint8_t Receiver::mfgIDL = 0x0;
uint8_t Receiver::mfgIDH = 0x0;
uint8_t Receiver::group = 0;
uint8_t Receiver::cmd = 0;
struct bt_le_scan_param Receiver::scanParam = {};

void Receiver::initHW(uint16_t mID, uint8_t grp)
{
    mfgIDH = mID >> 8;
    mfgIDL = mID;
    group = grp;

	scanParam.type = BT_LE_SCAN_TYPE_PASSIVE;
	scanParam.window_coded = 0;
	scanParam.interval_coded = 0;
	scanParam.timeout = 0;
	scanParam.options = BT_LE_SCAN_OPT_FILTER_DUPLICATE;
	scanParam.interval = BT_GAP_SCAN_FAST_INTERVAL;
	scanParam.window = BT_GAP_SCAN_FAST_WINDOW;

    int btErr = bt_enable(NULL);
    printk("BT enable error = %d\n",btErr);
}

void Receiver::listen()
{
    int scanErr = bt_le_scan_start(&scanParam,Receiver::onReceive);
	if (scanErr == 0)
		printk("Receiver is listening...\n");
	else
		printk("scan err = %d\n", scanErr);
}

uint8_t Receiver::receive()
{
    return cmd;
}
void Receiver::onReceive(const bt_addr_le_t *addr, 
                      int8_t rssi, 
                      uint8_t advType, 
                      struct net_buf_simple *buf)
{
	char manufData[MANUFACTURER_DATA_LEN];
	bt_data_parse(buf, Receiver::dataParser, manufData);
	if (manufData[0] == mfgIDL && manufData[1] == mfgIDH)
    {
        if (group == manufData[2])
        {
		    cmd = manufData[3];
		    printk("Group: %d, cmd: %d\n", manufData[2], manufData[3]);
        }
    }
}
bool Receiver::dataParser(struct bt_data *data, void *userData)
{
    char *name = (char*) userData;

	switch (data->type) {
	case BT_DATA_MANUFACTURER_DATA:
		memcpy(name, data->data, MIN(data->data_len, MANUFACTURER_DATA_LEN));
		return false;
	default:
		return true;
	}
}