#include "button/button.h"
#include "factory/factory.h"

Button::Button()
{
    pollTm.setDnd(true);
    pollTm.setId(Event::evTimeout);
    pollTm.setTarget(this);
    pollTm.setDelay(50);

    ev.setDnd(true);
    ev.setTarget(this);

    state = ST_INITIAL;
    pinState = LOW;
    oldPinState = LOW;
}

Button::~Button()
{

}
void Button::initHW(uint8_t p, const char* port)
{
    pin = p;
    int ret;
    driver = const_cast<device*> (device_get_binding(port));
    printk("driver of pin %02d is 0x%08x\n", pin, driver);    
    ret = gpio_pin_configure(driver,pin,GPIO_INPUT | GPIO_INT_DEBOUNCE | GPIO_ACTIVE_LOW);
    if (ret != 0)
        printk("gpio_pin_configure() Error! (pin: %d, error: %d)\n", pin, ret);

    pinState =  (PINSTATE) gpio_pin_get(driver,pin);
    oldPinState = pinState;
}
bool Button::processEvent(Event* event)
{
    bool processed = false;
    POLLSTATE oldstate = state;
    switch (state)
    {
        case ST_INITIAL:
        if (event->getId()==Event::evInitial)
        {
            state = ST_WAIT;
            
        }
        break;
        case ST_WAIT:
        if (event->getId() == Event::evTimeout)
        {
            state = ST_POLL;
        }
        break;
        case ST_POLL:
        if (event->getId() == Event::evDefault)
        {
            state = ST_WAIT;
        }
        break;
        default:
        break;
    }
    if (state != oldstate)
    {
        switch (state)
        {
            case ST_INITIAL:
            //printk("State Initial\n");
            break;
            case ST_WAIT:
            //printk("State Wait\n");
            Factory::xf()->pushEvent(&pollTm);
            break;
            case ST_POLL:
            //printk("State Poll\n");
            pinState = (PINSTATE) gpio_pin_get(driver,pin);
            if (pinState == HIGH && oldPinState == LOW)
            {
                //pressed
            }
            if (pinState == LOW && oldPinState == HIGH)
            {
                //released
            }
            if (pinState ==  oldPinState)
            {
                //no change
            }
            oldPinState = pinState;
            ev.setId(Event::evDefault);
            Factory::xf()->pushEvent(&ev);
            break;
            default:
            break;
        }
        processed = true;
    }
    return processed;
}
void Button::startBehaviour()
{
    ev.setId(Event::evInitial);
    Factory::xf()->pushEvent(&ev);
}

bool Button::pressed()
{
    bool retval = false;
    if (pinState == HIGH)
    {
        retval = true;
    }
    else
    {
        retval = false;
    }
    return retval;
}