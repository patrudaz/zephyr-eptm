#include "factory/factory.h"

#if (TRANSMITTER != 0) 
#endif
#if (RECEIVER != 0)
#endif

Factory::Factory()
{
}

Factory::~Factory()
{
}

void Factory::init() 
{
    #if (TRANSMITTER != 0)
    Transmitter::initHW(0x025a,MY_GROUP);
    #endif

    #if (RECEIVER != 0)
    Receiver::initHW(0x025a, MY_GROUP);
    #endif

    xf()->init();
}

void Factory::build()
{
}

void Factory::start()
{
    #if (TRANSMITTER != 0)
    #endif
}

