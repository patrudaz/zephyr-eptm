#ifndef MOTOR_ONCE
#define MOTOR_ONCE

#include <stdint.h>
#include <zephyr.h>
#include <drivers/gpio.h>

class Motor
{
public:
    Motor();
    ~Motor();
    void initHW(uint8_t pin1, const char* port1,
                uint8_t pin2, const char* port2);
    void forward();
    void backward();
    void stop();

private: 
    struct device* driver1;
    struct device* driver2;
    uint8_t pin1;
    uint8_t pin2;
};
#endif