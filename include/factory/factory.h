#ifndef FACTORY_ONCE
#define FACTORY_ONCE

#include "xf/xf.h"
#include "button/button.h"
#include "motor/motor.h"
#include "radio/transmitter.h"
#include "radio/receiver.h"

#define TRANSMITTER 0
#define RECEIVER 0
#define MY_GROUP 1

class Factory
{
public:
    static void init();
    static void build();
    static void start();
    #if (TRANSMITTER != 0)

    #endif

    #if (RECEIVER != 0)
    #endif

    static XF* xf() {return XF::getInstance();}

private:
    Factory(/* args */);
    ~Factory();

    #if (TRANSMITTER != 0)
    #endif

    #if (RECEIVER != 0)
    #endif
};

#endif