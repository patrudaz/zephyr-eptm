#include <zephyr.h>
#include "ireactive.h"
#include <stdint.h>

#ifndef EVENT_ONCE
#define EVENT_ONCE


class Event
{
public:
    typedef enum evID { evNone, 
                        evInitial, evTimeout, evDefault, 
                        evPressed, evReleased, 
                        evForward, evReverse, evStop , 
                        evBegin, evEnd, 
                        evMessage,
                        evDecode 
                       } evID;
    Event(/* args */);
    Event(evID eventID);
    ~Event();
    void setTarget(IReactive* target);
    IReactive* getTarget();
    void setId(evID eventID);
    evID getId();
    void setDnd(bool doNotDelete);
    bool getDnd();
    void setDelay(int delay);
    int getDelay();
    void setT(struct k_timer* timer);
    struct k_timer* getT();
    void setEventData(uint8_t data);
    uint8_t getEventData();

private:
    evID id;
    IReactive* target;
    struct k_timer* t;
    bool deleteAfterConsume;
    int delay;
    uint8_t eventData;
};

#endif