#ifndef BUTTON_ONCE
#define BUTTON_ONCE

#include <stdint.h>
#include <zephyr.h>
#include "../xf/xf.h"
#include <drivers/gpio.h>

class Button : public IReactive
{
public:
    typedef enum POLLSTATE
    {
        ST_INITIAL,
        ST_WAIT,
        ST_POLL
    } BSTATE;
    typedef enum PINSTATE
    {
        LOW,
        HIGH
    } PINSTATE;
    Button();
    ~Button();
    void initHW(uint8_t pin, const char* port);
    bool processEvent(Event* event);
    void startBehaviour();
    bool pressed();

private: 
    Event pollTm;
    Event ev;
    POLLSTATE state;
    PINSTATE pinState;
    PINSTATE oldPinState;  
    struct device* driver; 
    uint8_t pin;
};
#endif